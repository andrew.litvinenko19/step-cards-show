/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 22);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),
/* 1 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
var stylesInDom = {},
	memoize = function(fn) {
		var memo;
		return function () {
			if (typeof memo === "undefined") memo = fn.apply(this, arguments);
			return memo;
		};
	},
	isOldIE = memoize(function() {
		return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
	}),
	getHeadElement = memoize(function () {
		return document.head || document.getElementsByTagName("head")[0];
	}),
	singletonElement = null,
	singletonCounter = 0,
	styleElementsInsertedAtTop = [];

module.exports = function(list, options) {
	if(typeof DEBUG !== "undefined" && DEBUG) {
		if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};
	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (typeof options.singleton === "undefined") options.singleton = isOldIE();

	// By default, add <style> tags to the bottom of <head>.
	if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

	var styles = listToStyles(list);
	addStylesToDom(styles, options);

	return function update(newList) {
		var mayRemove = [];
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			domStyle.refs--;
			mayRemove.push(domStyle);
		}
		if(newList) {
			var newStyles = listToStyles(newList);
			addStylesToDom(newStyles, options);
		}
		for(var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];
			if(domStyle.refs === 0) {
				for(var j = 0; j < domStyle.parts.length; j++)
					domStyle.parts[j]();
				delete stylesInDom[domStyle.id];
			}
		}
	};
}

function addStylesToDom(styles, options) {
	for(var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];
		if(domStyle) {
			domStyle.refs++;
			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}
			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];
			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}
			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles(list) {
	var styles = [];
	var newStyles = {};
	for(var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};
		if(!newStyles[id])
			styles.push(newStyles[id] = {id: id, parts: [part]});
		else
			newStyles[id].parts.push(part);
	}
	return styles;
}

function insertStyleElement(options, styleElement) {
	var head = getHeadElement();
	var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
	if (options.insertAt === "top") {
		if(!lastStyleElementInsertedAtTop) {
			head.insertBefore(styleElement, head.firstChild);
		} else if(lastStyleElementInsertedAtTop.nextSibling) {
			head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			head.appendChild(styleElement);
		}
		styleElementsInsertedAtTop.push(styleElement);
	} else if (options.insertAt === "bottom") {
		head.appendChild(styleElement);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement(styleElement) {
	styleElement.parentNode.removeChild(styleElement);
	var idx = styleElementsInsertedAtTop.indexOf(styleElement);
	if(idx >= 0) {
		styleElementsInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement(options) {
	var styleElement = document.createElement("style");
	styleElement.type = "text/css";
	insertStyleElement(options, styleElement);
	return styleElement;
}

function createLinkElement(options) {
	var linkElement = document.createElement("link");
	linkElement.rel = "stylesheet";
	insertStyleElement(options, linkElement);
	return linkElement;
}

function addStyle(obj, options) {
	var styleElement, update, remove;

	if (options.singleton) {
		var styleIndex = singletonCounter++;
		styleElement = singletonElement || (singletonElement = createStyleElement(options));
		update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
		remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
	} else if(obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function") {
		styleElement = createLinkElement(options);
		update = updateLink.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
			if(styleElement.href)
				URL.revokeObjectURL(styleElement.href);
		};
	} else {
		styleElement = createStyleElement(options);
		update = applyToTag.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
		};
	}

	update(obj);

	return function updateStyle(newObj) {
		if(newObj) {
			if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
				return;
			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;
		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag(styleElement, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (styleElement.styleSheet) {
		styleElement.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = styleElement.childNodes;
		if (childNodes[index]) styleElement.removeChild(childNodes[index]);
		if (childNodes.length) {
			styleElement.insertBefore(cssNode, childNodes[index]);
		} else {
			styleElement.appendChild(cssNode);
		}
	}
}

function applyToTag(styleElement, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		styleElement.setAttribute("media", media)
	}

	if(styleElement.styleSheet) {
		styleElement.styleSheet.cssText = css;
	} else {
		while(styleElement.firstChild) {
			styleElement.removeChild(styleElement.firstChild);
		}
		styleElement.appendChild(document.createTextNode(css));
	}
}

function updateLink(linkElement, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	if(sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = linkElement.href;

	linkElement.href = URL.createObjectURL(blob);

	if(oldSrc)
		URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["c"] = makeCardCarcass;
/* harmony export (immutable) */ __webpack_exports__["d"] = makeCardiologistBlock;
/* harmony export (immutable) */ __webpack_exports__["a"] = makeDentistBlock;
/* harmony export (immutable) */ __webpack_exports__["b"] = makeTherapistBlock;
const DEFAULT_MODAL = (id) => (`
    <div class="modal fade" id=${id} data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content my-window">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>
    `)
/* harmony export (immutable) */ __webpack_exports__["e"] = DEFAULT_MODAL;



function makeCardCarcass(id, color, fio, doctor){
    const carcass = `<div id = '${id}' class="card text-white mb-3" style= "background: ${color}">
            <div class="card-header">Card of patient</div>
            <svg class="edit-img" height="401pt" viewBox="0 -1 401.52289 401" width="401pt" xmlns="http://www.w3.org/2000/svg"><path d="m370.589844 250.972656c-5.523438 0-10 4.476563-10 10v88.789063c-.019532 16.5625-13.4375 29.984375-30 30h-280.589844c-16.5625-.015625-29.980469-13.4375-30-30v-260.589844c.019531-16.558594 13.4375-29.980469 30-30h88.789062c5.523438 0 10-4.476563 10-10 0-5.519531-4.476562-10-10-10h-88.789062c-27.601562.03125-49.96875 22.398437-50 50v260.59375c.03125 27.601563 22.398438 49.96875 50 50h280.589844c27.601562-.03125 49.96875-22.398437 50-50v-88.792969c0-5.523437-4.476563-10-10-10zm0 0"/><path d="m376.628906 13.441406c-17.574218-17.574218-46.066406-17.574218-63.640625 0l-178.40625 178.40625c-1.222656 1.222656-2.105469 2.738282-2.566406 4.402344l-23.460937 84.699219c-.964844 3.472656.015624 7.191406 2.5625 9.742187 2.550781 2.546875 6.269531 3.527344 9.742187 2.566406l84.699219-23.464843c1.664062-.460938 3.179687-1.34375 4.402344-2.566407l178.402343-178.410156c17.546875-17.585937 17.546875-46.054687 0-63.640625zm-220.257812 184.90625 146.011718-146.015625 47.089844 47.089844-146.015625 146.015625zm-9.40625 18.875 37.621094 37.625-52.039063 14.417969zm227.257812-142.546875-10.605468 10.605469-47.09375-47.09375 10.609374-10.605469c9.761719-9.761719 25.589844-9.761719 35.351563 0l11.738281 11.734375c9.746094 9.773438 9.746094 25.589844 0 35.359375zm0 0"/></svg>
            <div class="card-body">
            <div class="first-half">
                <h5 class="card-title">Patient: <br/> ${fio}</h5>
                <p class="card-text" id="doc">Doctor: ${doctor}</p>
            </div>
            </div>
        </div>`
    return carcass
}


function makeCardiologistBlock(urgency, target, description, diseases, bodyMassIndex, normalPressure, age, comments){
    const cardioBlock = `<div class="second-half">
                <p class="card-text">Target-urgency: ${urgency}</p>
                <p class="card-text">Visit-target: ${target}</p>
                <p class="card-text">Description: ${description}</p>
                <p class="card-text">Diseases: ${diseases}</p>
                <p class="card-text">Body index mass: ${bodyMassIndex}</p>
                <p class="card-text">Pressure: ${normalPressure}</p>
                <p class="card-text">Age: ${age}</p>
                <p class="card-text">Comments: ${comments}</p>
         </div>`
    return cardioBlock
}

function makeDentistBlock(urgency, target, description, lastVisit, comments){
    const dentistBlock = `<div class="second-half">
                <p class="card-text">Target-urgency: ${urgency}</p>
                <p class="card-text">Visit-target: ${target}</p>
                <p class="card-text">Description: ${description}</p>
                <p class="card-text">Last visit: ${lastVisit}</p>
                <p class="card-text">Comments: ${comments}</p>
        </div>`
    return dentistBlock
}

function makeTherapistBlock(urgency, target, description, age, comments){
    const therapistBlock = `<div class="second-half">
                <p class="card-text">Target-urgency: ${urgency}</p>
                <p class="card-text">Visit-target: ${target}</p>
                <p class="card-text">Description: ${description}</p>
                <p class="card-text">Age: ${age}</p>
                <p class="card-text">Comments: ${comments}</p>
        </div>`
    return therapistBlock
}

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Visit {
    constructor(target, description, urgency, fullName, doctor, comments) {
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.comments = comments
        this.doctor = doctor
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Visit;


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class API {
    static async auth(user) {
        return await fetch('https://ajax.test-danit.com/api/v2/cards/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
            body: JSON.stringify(user)
        })
            .then(r => {
                if (r.status >= 400) {
                    throw new Error('wrong creds')
                } else return r
            })
            .then(response => response.text())
    }

    static async addCard(card) {
        const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
            body: JSON.stringify(card)
        }).then(res => res.json())
        return response
    }

    static async getAllCards() {
           let resp = await fetch('https://ajax.test-danit.com/api/v2/cards', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
        }).then(res => res.json())
        return await resp
    }

    static async removeCards(id) {
        return await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
        }).then(res => res.text())
    }

    static async editCard(newCard) {
        return await fetch(`https://ajax.test-danit.com/api/v2/cards/${newCard.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            },
            body: JSON.stringify(newCard)
        })
            .then(response => response.json())
    }
    static async getSingleCard(ID){
        return await fetch(`https://ajax.test-danit.com/api/v2/cards/${ID}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('userToken')}`
            }
        }).then(res => res.json())
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = API;



/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__API_API_js__ = __webpack_require__(4);


class Filters {


    makeFilter() {
        __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].getAllCards().then(cards => {
            const cardsElem = document.querySelectorAll('.card');
            const filters = this.readFilters();
            const filteredCards = this.filterCards(cards, filters);
            const filteredCardsId = filteredCards.map(item => item.id);
            this.showCard(cardsElem, filteredCardsId);
        })
    }

    readFilters() {
        const filters = {};
        const urgencySelectValue = document.querySelector(".urgency-select").value;
        const doctorSelectValue = document.querySelector(".doctor-select").value;
        const descriptionValue = document.querySelector(".search-string").value;

        if (urgencySelectValue !== 'Urgency') {
            filters.urgency = urgencySelectValue;
        }
        if (doctorSelectValue !== 'Doctors') {
            filters.doctor = doctorSelectValue;
        }
        if (!!descriptionValue){
            filters.description = descriptionValue;
        }
        return filters;
    }

    filterCards(cards, filters) {
        return cards.filter(card => {
            for (const key in filters) {
                if ((key === 'urgency' || key === 'doctor') && filters[key] === 'showAll') continue;
                if (key === 'description') {
                    if (!card[key].includes(filters[key])) return false
                    continue;
                }
                if (card[key] !== filters[key]) return false
            }
            return true;
        })
    }

    showCard(allCards, filteredCardsId) {
        allCards.forEach(card => {
            if (filteredCardsId.includes(+card.getAttribute('id'))) {
                if (card.style.display !== 'block') card.style.display = 'block';
            } else {
                card.style.display = 'none'
            }
        })
    }

    setUpFilter() {
        const urgencySelectValue = document.querySelector(".urgency-select");
        const doctorSelectValue = document.querySelector(".doctor-select");
        const descriptionValue = document.querySelector(".search-string");
        urgencySelectValue.addEventListener('change',this.makeFilter.bind(this))
        doctorSelectValue.addEventListener('change', this.makeFilter.bind(this))
        descriptionValue.addEventListener('keydown', this.makeFilter.bind(this))
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Filters;


/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__API_API_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__visit_visitTherapist_js__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__visit_visitCardiologist_js__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__visit_visitDentist__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__filters_filter_js__ = __webpack_require__(5);








class Form {

    constructor() {
        this.ELEMENTS = {
            wrapper: document.createElement('div'),
            visitWrapper: document.createElement('form'),
            selectWrapper: document.createElement('div'),
            email: document.createElement('input'),
            password: document.createElement('input'),
            btn: document.createElement('button'),
            errorContainer: document.createElement('div'),
            error: document.createElement('p'),
            fullName: document.createElement('input'),
            purpose: document.createElement('input'),
            description: document.createElement('input'),


            formSelect: document.createElement('select'),
            formSelectOption1: document.createElement('option'),
            formSelectOption2: document.createElement('option'),
            formSelectOption3: document.createElement('option'),
            formSelectOption4: document.createElement('option'),


            doctorSelect: document.createElement('select'),
            doctorSelectOption1: document.createElement('option'),
            doctorSelectOption2: document.createElement('option'),
            doctorSelectOption3: document.createElement('option'),
            doctorSelectOption4: document.createElement('option'),


            pressure: document.createElement('input'),
            index: document.createElement('input'),
            diseases: document.createElement('input'),
            age: document.createElement('input'),
            lastDate: document.createElement('input'),
            dentistParent: document.createElement('div'),
            therapistParent: document.createElement('div'),
            cardiologistParent: document.createElement('div'),

            createButton: document.createElement('button'),

            comments: document.createElement('input'),

            editButton: document.createElement('button'),
        }
    }


    render() {
        const {wrapper, email, password, btn, errorContainer, error} = this.ELEMENTS

        wrapper.classList.add('my-wrapper')
        btn.classList.add('my-btn')
        btn.classList.add('my-submit')
        btn.innerText = 'LOG IN'
        error.classList.add('error-text')
        error.innerText = ''
        email.placeholder = 'Enter your email'
        email.classList.add('my-input')
        email.value = ''
        password.classList.add('my-input')
        password.placeholder = 'Enter your password'
        password.value = ''
        password.type = 'password'
        wrapper.append(email, password, errorContainer, btn)

        return wrapper;
    }


    handleClick(modalID) {
        const {email, password, error, btn, errorContainer} = this.ELEMENTS

        btn.addEventListener('click', (eve) => {
            eve.preventDefault()
            error.classList.add('error-text')

            __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].auth({email: `${email.value}`, password: `${password.value}`})
                .then(token => {
                    localStorage.setItem('userToken', token)
                    error.remove()

                    document.querySelector('#add-btn').classList.add('active-btn')
                    document.querySelector('#img').style.display = 'none'

                    document.querySelector('#log-in').classList.remove('active-btn')
                    document.querySelector('#log-out').classList.add('active-btn')

                    this.checkCards()
                    document.querySelector(`#${modalID} .btn-close`).click()
                    document.querySelector('.filters').style.display = 'flex'
                    const filter = new __WEBPACK_IMPORTED_MODULE_5__filters_filter_js__["a" /* Filters */]()
                })
                .catch(e => {
                    error.innerText = `You entered wrong email or password... 
                Try again`
                    errorContainer.append(error)
                    localStorage.removeItem('userToken')
                    console.error(e)
                })
        })
    }

    async checkCards() {
        const cards = await __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].getAllCards()
        if (cards.length > 0) {
            cards.forEach(card => {
                if (card.doctor === 'Cardiologist') {
                    const oldCardio = new __WEBPACK_IMPORTED_MODULE_3__visit_visitCardiologist_js__["a" /* VisitCardiologist */](card.target, card.description, card.urgency, card.fullName, card.doctor, card.comments, card.pressure, card.bodyMassIndex, card.diseases, card.age)
                    const newCard = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](card, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(card.urgency), oldCardio)
                    newCard.render(document.body.querySelector('.card-list'))
                } else if (card.doctor === 'Therapist') {
                    const oldTherapi = new __WEBPACK_IMPORTED_MODULE_2__visit_visitTherapist_js__["a" /* VisitTherapist */](card.target, card.description, card.urgency, card.fullName, card.doctor, card.age, card.comments)
                    const newCard = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](card, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(card.urgency), oldTherapi)
                    newCard.render(document.body.querySelector('.card-list'))
                } else {
                    const oldDanti = new __WEBPACK_IMPORTED_MODULE_4__visit_visitDentist__["a" /* VisitDentist */](card.target, card.description, card.urgency, card.fullName, card.doctor, card.lastDate, card.comments)
                    const newCard = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](card, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(card.urgency), oldDanti)
                    newCard.render(document.body.querySelector('.card-list'))
                }
            })
        } else {
            document.querySelector('#msg').classList.add('active-msg')
        }

    }

    visit(parent) {
        const {
            fullName,
            description,
            purpose,
            formSelect,
            formSelectOption1,
            formSelectOption2,
            formSelectOption3,
            formSelectOption4,
            visitWrapper,
            doctorSelect,
            doctorSelectOption1,
            doctorSelectOption2,
            doctorSelectOption3,
            doctorSelectOption4,
            selectWrapper,
            createButton,
            pressure,
            index,
            diseases,
            age,
            comments,
            errorContainer,
        } = this.ELEMENTS
        fullName.placeholder = 'Full Name'
        fullName.value = ''
        fullName.classList.add('visit-input')
        fullName.maxLength = 35

        pressure.value = ''
        diseases.value = ''
        index.value = ''
        age.value = ''

        description.placeholder = 'description'
        description.value = ''
        description.classList.add('visit-input')
        description.maxLength = 100

        purpose.placeholder = 'purpose of the visit'
        purpose.value = ''
        purpose.classList.add('visit-input')
        purpose.maxLength = 50

        formSelect.classList.add('visit-input')
        formSelect.classList.add('my-select')
        formSelectOption1.innerText = 'Urgency'
        formSelectOption1.setAttribute('disabled', 'disabled')
        formSelectOption1.setAttribute('selected', 'selected')

        formSelectOption2.innerText = 'Low'
        formSelectOption3.innerText = 'Normal'
        formSelectOption4.innerText = 'High'

        formSelect.append(formSelectOption1, formSelectOption2, formSelectOption3, formSelectOption4)

        doctorSelect.classList.add('visit-input')
        doctorSelect.classList.add('my-select')
        doctorSelectOption1.innerText = 'Doctors'
        doctorSelectOption1.setAttribute('disabled', 'disabled')
        doctorSelectOption1.setAttribute('selected', 'selected')

        doctorSelectOption2.innerText = 'Cardiologist'
        doctorSelectOption3.innerText = 'Dentist'
        doctorSelectOption4.innerText = 'Therapist'

        doctorSelect.append(doctorSelectOption1, doctorSelectOption2, doctorSelectOption3, doctorSelectOption4)

        comments.placeholder = 'Add your comments'
        comments.classList.add('visit-input')
        comments.value = ''
        comments.maxLength = 100

        createButton.innerText = 'CREATE CARD'
        createButton.classList.add('my-btn')
        createButton.classList.add('create-btn')

        visitWrapper.classList.add('visit-wrapper')
        visitWrapper.append(fullName, description, purpose, formSelect, doctorSelect, selectWrapper, comments, errorContainer, createButton)
        parent.append(visitWrapper)
        this.showDoctor()
        this.createCard(createButton, 'card-modal')
    }

    createCard(button, modalID) {
        const {
            doctorSelect,
            purpose,
            description,
            formSelect,
            fullName,
            pressure,
            index,
            diseases,
            age,
            lastDate,
            errorContainer,
            error,
            comments
        } = this.ELEMENTS

        button.addEventListener('click', (e) => {
            e.preventDefault()
            const filter = new __WEBPACK_IMPORTED_MODULE_5__filters_filter_js__["a" /* Filters */]()

            if (doctorSelect.selectedIndex === 1 && fullName.value !== '' && description.value !== '' && pressure.value !== '' && diseases.value !== '' && index.value !== '' && age.value !== '') {
                error.remove()
                const cardiologistObj = new __WEBPACK_IMPORTED_MODULE_3__visit_visitCardiologist_js__["a" /* VisitCardiologist */](purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, comments.value, pressure.value, index.value, diseases.value, age.value)
                document.querySelector(`#${modalID} .btn-close`).click()
                __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].addCard(cardiologistObj).then(data => {
                    const card = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](data, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(formSelect.value), cardiologistObj)
                    card.render(document.body.querySelector('.card-list'))
                })

            } else if (doctorSelect.selectedIndex === 2 && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && lastDate.value !== '') {
                error.remove()
                const dentistObj = new __WEBPACK_IMPORTED_MODULE_4__visit_visitDentist__["a" /* VisitDentist */](purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, lastDate.value, comments.value)
                document.querySelector(`#${modalID} .btn-close`).click()
                __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].addCard(dentistObj).then(data => {
                    const card = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](data, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(formSelect.value), dentistObj)
                    card.render(document.body.querySelector('.card-list'))
                })

            } else if (doctorSelect.selectedIndex === 3 && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && age.value !== '') {
                error.remove()
                const therapistObj = new __WEBPACK_IMPORTED_MODULE_2__visit_visitTherapist_js__["a" /* VisitTherapist */](purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, age.value, comments.value)
                document.querySelector(`#${modalID} .btn-close`).click()
                __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].addCard(therapistObj).then(data => {
                    const card = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](data, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(formSelect.value), therapistObj)
                    card.render(document.body.querySelector('.card-list'))
                })

            } else {
                error.classList.add('error-text')
                error.innerText = 'Fill in all the fields'
                errorContainer.append(error)
            }
        })

    }

    cardiologist(parent) {
        const {pressure, diseases, index, age, cardiologistParent} = this.ELEMENTS
        pressure.placeholder = 'pressure'
        pressure.classList.add('visit-input')

        diseases.placeholder = 'diseases of the cardiovascular system'
        diseases.classList.add('visit-input')

        index.placeholder = 'index'
        index.classList.add('visit-input')

        age.placeholder = 'age'
        age.classList.add('visit-input')

        cardiologistParent.classList.add('visit-wrapper')
        cardiologistParent.append(pressure, diseases, index, age)
        parent.append(cardiologistParent)
    }

    dentist(parent) {
        const {lastDate, dentistParent} = this.ELEMENTS
        lastDate.placeholder = 'date of the last visit'
        lastDate.classList.add('visit-input')
        lastDate.type = 'date'


        dentistParent.append(lastDate)
        parent.append(dentistParent)

    }

    therapist(parent) {
        const {age, therapistParent} = this.ELEMENTS
        age.placeholder = 'age'
        age.classList.add('visit-input')
        therapistParent.append(age)
        parent.append(therapistParent)
    }

    showDoctor() {
        const {
            doctorSelect,
            dentistParent,
            therapistParent,
            cardiologistParent,
            selectWrapper,
        } = this.ELEMENTS
        doctorSelect.addEventListener('change', () => {
            if (doctorSelect.selectedIndex === 2) {
                therapistParent.remove()
                cardiologistParent.remove()
                this.dentist(selectWrapper)

            } else if (doctorSelect.selectedIndex === 1) {
                dentistParent.remove()
                therapistParent.remove()
                this.cardiologist(selectWrapper)

            } else if (doctorSelect.selectedIndex === 3) {
                dentistParent.remove()
                cardiologistParent.remove()

                this.therapist(selectWrapper)
            }
        })
    }

    async editCard(cardID) {
        const {
            createButton,
            editButton,
            visitWrapper,
            fullName,
            description,
            purpose,
            formSelect,
            doctorSelect,
            comments,
            pressure,
            diseases,
            index,
            age,
            lastDate,
            selectWrapper,
            error, errorContainer
        } = this.ELEMENTS

        createButton.remove()
        editButton.innerText = 'EDIT CARD'
        editButton.classList.add('my-btn')
        editButton.classList.add('edit-btn')

        visitWrapper.append(editButton)

        const oldCard = await __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].getSingleCard(cardID)

        fullName.value = oldCard.fullName
        description.value = oldCard.description
        purpose.value = oldCard.target
        formSelect.value = oldCard.urgency
        doctorSelect.value = oldCard.doctor
        comments.value = oldCard.comments

        if (doctorSelect.value === 'Cardiologist') {
            pressure.value = oldCard.normalPressure
            diseases.value = oldCard.diseases
            index.value = oldCard.bodyMassIndex
            age.value = oldCard.age

            this.cardiologist(selectWrapper)

        } else if (doctorSelect.value === 'Dentist') {
            lastDate.value = oldCard.lastVisit

            this.dentist(selectWrapper)

        } else if (doctorSelect.value === 'Therapist') {
            age.value = oldCard.age

            this.therapist(selectWrapper)

        }

        document.querySelector('#card-modal').querySelector('.btn-close').addEventListener('click', () => {
            document.querySelector('.modal').style.display = 'none'
            document.querySelector('#card-modal').classList.remove('show')
        })

        editButton.addEventListener('click', (e) => {
            e.preventDefault()
            const filter = new __WEBPACK_IMPORTED_MODULE_5__filters_filter_js__["a" /* Filters */]()

            if (doctorSelect.value === 'Therapist' && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && age.value !== '') {
                error.remove()
                const newCard = new __WEBPACK_IMPORTED_MODULE_2__visit_visitTherapist_js__["a" /* VisitTherapist */](purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, age.value, comments.value)
                newCard.id = cardID

                __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].editCard(newCard).then(data => {
                    document.getElementById(`${cardID}`).remove()
                    const card = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](data, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(formSelect.value), newCard)
                    card.render(document.body.querySelector('.card-list'))
                    let parent = document.getElementById(`${cardID}`)
                    parent.querySelector('.card-header').insertAdjacentHTML('beforeend', `<span class="grey">  [edited]</span>`)
                })

                document.querySelector('.modal').style.display = 'none'
                document.querySelector('#card-modal').classList.remove('show')
                filter.find()


            } else if (doctorSelect.value === 'Dentist' && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && lastDate.value !== '') {

                error.remove()

                const newCard = new __WEBPACK_IMPORTED_MODULE_4__visit_visitDentist__["a" /* VisitDentist */](purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, lastDate.value, comments.value)
                newCard.id = cardID

                __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].editCard(newCard).then(data => {
                    document.getElementById(`${cardID}`).remove()
                    const card = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](data, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(formSelect.value), newCard)
                    card.render(document.body.querySelector('.card-list'))
                    let parent = document.getElementById(`${cardID}`)
                    parent.querySelector('.card-header').insertAdjacentHTML('beforeend', `<span class="grey">[edited]</span>`)
                })

                document.querySelector('.modal').style.display = 'none'
                document.querySelector('#card-modal').classList.remove('show')
                filter.find()


            } else if (doctorSelect.value === 'Cardiologist' && fullName.value !== '' && description.value !== '' && purpose.value !== '' && formSelect.value !== 'Urgency' && doctorSelect.value !== 'Doctors' && pressure.value !== '' && diseases.value !== '' && index.value !== '' && age.value !== '') {

                error.remove()

                const newCard = new __WEBPACK_IMPORTED_MODULE_3__visit_visitCardiologist_js__["a" /* VisitCardiologist */](purpose.value, description.value, formSelect.value, fullName.value, doctorSelect.value, comments.value, pressure.value, index.value, diseases.value, age.value)
                newCard.id = cardID
                __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].editCard(newCard).then(data => {
                    document.getElementById(`${cardID}`).remove()
                    const card = new __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */](data, __WEBPACK_IMPORTED_MODULE_1__Card_Card_js__["a" /* Card */].prototype.madeColor(formSelect.value), newCard)
                    card.render(document.body.querySelector('.card-list'))
                    let parent = document.getElementById(`${cardID}`)
                    parent.querySelector('.card-header').insertAdjacentHTML('beforeend', `<span class="grey">[edited]</span>`)
                })

                document.querySelector('.modal').style.display = 'none'
                document.querySelector('#card-modal').classList.remove('show')
                filter.find()

            } else {
                error.classList.add('error-text')
                error.innerText = 'Fill in all the fields'
                errorContainer.append(error)
            }
        })

    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Form;




/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__visit_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constans_index_js__ = __webpack_require__(2);



class VisitCardiologist extends __WEBPACK_IMPORTED_MODULE_0__visit_js__["a" /* default */] {
    constructor(
        target,
        description,
        urgency,
        fullName,
        doctor,
        comments,
        normalPressure,
        bodyMassIndex,
        diseases,
        age) {
        super(target, description, urgency, fullName, doctor, comments);
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.doctor = doctor
        this.age = age
        this.bodyMassIndex = bodyMassIndex
        this.diseases = diseases
        this.normalPressure = normalPressure
        this.comments = comments
    }

    showCardiologist(parent) {
        parent.insertAdjacentHTML('afterend', `
        ${__WEBPACK_IMPORTED_MODULE_1__constans_index_js__["d" /* makeCardiologistBlock */](this.urgency, this.target, this.description, this.diseases, this.bodyMassIndex, this.normalPressure, this.age, this.comments)}      
         `)
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = VisitCardiologist;



/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__visit_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constans_index_js__ = __webpack_require__(2);



class VisitDentist extends __WEBPACK_IMPORTED_MODULE_0__visit_js__["a" /* default */] {
    constructor(
        target,
        description,
        urgency,
        fullName,
        doctor,
        lastVisit, comments) {
        super(target, description, urgency, fullName, doctor, comments);
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.doctor = doctor
        this.lastVisit = lastVisit
        this.comments = comments
    }

    showDentist(parent) {
        parent.insertAdjacentHTML('afterend', `${__WEBPACK_IMPORTED_MODULE_1__constans_index_js__["a" /* makeDentistBlock */](this.urgency, this.target, this.description, this.lastVisit, this.comments)}
        `)
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = VisitDentist;


/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__visit_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constans_index_js__ = __webpack_require__(2);



class VisitTherapist extends __WEBPACK_IMPORTED_MODULE_0__visit_js__["a" /* default */] {
    constructor(
        target,
        description,
        urgency,
        fullName,
        doctor,
        age,
        comments) {
        super(target, description, urgency, fullName, doctor, comments);
        this.target = target
        this.description = description
        this.urgency = urgency
        this.fullName = fullName
        this.doctor = doctor
        this.age = age
    }

    showTherapist(parent) {
        parent.insertAdjacentHTML('afterend', `
        ${__WEBPACK_IMPORTED_MODULE_1__constans_index_js__["b" /* makeTherapistBlock */](this.urgency, this.target, this.description, this.age, this.comments )}
        `)
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = VisitTherapist;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(16);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(1)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./card.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./card.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(17);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(1)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./filters.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./filters.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(18);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(1)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./first_screen.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./first_screen.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(19);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(1)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./form.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./form.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(20);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(1)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/css-loader/index.js!./formVisit.css", function() {
			var newContent = require("!!../../node_modules/css-loader/index.js!./formVisit.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__constans__ = __webpack_require__(2);


class Modal {
    constructor(id) {
        this.id = id
        this._ELEMENTS = {
            window: __WEBPACK_IMPORTED_MODULE_0__constans__["e" /* DEFAULT_MODAL */](id)
        }
    }

    render() {
        const {window} = this._ELEMENTS
        const parent = document.querySelector('.modal-container')

        parent.insertAdjacentHTML('afterbegin', window)
    }

    appendBody(content) {
        const parent = document.querySelector(`#${this.id} .modal-body`);
        parent.append(content);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Modal;



/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, ".card-list{display:flex;justify-content:center;align-items:flex-start;flex-wrap:wrap}.card{overflow:hidden;box-shadow:inset 0 -3px 8px rgba(0,0,0,.6),inset 0 3px 8px rgba(252,255,255,.7),0 3px 8px -3px rgba(0,0,0,.8);width:320px;margin:5px 10px;border:none}.delete-card{position:absolute;right:5px;top:4px;z-index:1000}.btn-more{position:absolute;left:50%;bottom:-10px;transform:translate(-50%);height:20px;width:20px;margin-bottom:5px;clip-path:polygon(50% 3%,0 68%,100% 68%);background:linear-gradient(180deg,#030 0,#036)}.show-more{clip-path:polygon(47% 100%,1% 26%,100% 26%);bottom:-3%}.card-title{font-family:Varela Round,sans-serif;font-size:26px;color:#232323}.card-header{font-size:15px}.card-header,.card-text{color:#232323;font-style:italic;font-weight:700}.card-text{font-size:18px;font-family:Varela Round,sans-serif;line-height:40px}.first-half{display:block}.second-half{display:none}.edit-img{width:22px;height:22px;position:absolute;top:3px;right:40px;cursor:pointer;z-index:1000}.grey{color:#a9a9a9;margin-left:5px}", ""]);

// exports


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, ".filters{display:none;justify-content:center;gap:50px;align-items:center;margin:30px auto 50px}.selects{display:flex;justify-content:space-between;align-content:center;width:1000px;margin:30px auto 0;gap:50px}.autocomplete{position:relative;display:flex}.form-select{background:#b4dfee}.urgency-select{clip-path:polygon(90% 0,100% 50%,90% 100%,0 100%,10% 50%,0 0)}.doctor-select,.urgency-select{width:300px;padding:5px 50px;cursor:pointer}.doctor-select{clip-path:polygon(100% 0,90% 50%,100% 100%,10% 100%,0 50%,10% 0)}.search-btn{width:150px}.search-btn,.search-string{height:45px;border:none;background:#b4dfee;outline:none}.search-string{width:450px;padding:0 20px}.autocomplete-items{position:absolute;border:1px solid #d4d4d4;border-bottom:none;border-top:none;z-index:99;top:100%;left:0;right:0}.autocomplete-items div{padding:10px;cursor:pointer;background-color:#fff;border-bottom:1px solid #d4d4d4}.autocomplete-items div:hover{background-color:#e9e9e9}.autocomplete-active{background-color:#1e90ff!important;color:#fff}", ""]);

// exports


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, ".my-container{background:#171932}.my-nav-container{width:100%;background:#030013}.my-nav,.my-nav-container{margin:0 auto;height:100px}.my-nav{width:90%;display:flex;justify-content:space-between;align-items:center}.logo{color:#b4dfee;font-size:50px}.logo,.my-btn{text-decoration:none;font-weight:700;font-family:Zen Tokyo Zoo,cursive}.my-btn{display:none;padding:15px 20px;margin:10px 20px;border-radius:10px;box-shadow:inset 0 0 40px 40px #acc9d1,0 0 0 0 #b4dfee;letter-spacing:2px;color:#232653;transition:.15s ease-in-out;width:150px}.active-btn{display:inline-block}.my-btn:hover{box-shadow:inset 0 0 10px 0 #acc9d1,0 0 10px 4px #b4dfee;color:#232653}.img-container{width:100%;display:flex;justify-content:center;align-items:center;margin-top:70px}.bg-img{width:500px;height:500px}.add-btn{width:170px;margin:0 auto 40px}.my-message{display:none;font-weight:700;font-size:30px;color:#acc9d1}.active-msg{display:block}", ""]);

// exports


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, ".my-window{border-radius:10px;background:#567599;background:-webkit-linear-gradient(90deg,#567599,#1f2e4b);background:linear-gradient(180deg,#567599,#1f2e4b);box-shadow:inset 0 -3px 8px rgba(0,0,0,.6),inset 0 3px 8px rgba(252,255,255,.7),0 3px 8px -3px rgba(0,0,0,.8)}.my-wrapper{display:flex;flex-direction:column}.my-submit{display:inline-block;margin:40px auto 0;font-size:20px;width:150px;height:60px;box-sizing:border-box;text-align:center;padding:0}.my-input{width:450px;height:50px;border-radius:10px;background:linear-gradient(90deg,#567599,#1f2e4b);outline:none;border:none;color:#030013;padding:20px;margin-top:25px}.my-input::placeholder{color:#030013}.error-text{color:#cd5c5c;text-align:center;font-size:20px;font-weight:700;font-style:italic;margin-top:20px}", ""]);

// exports


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, ".create-btn{display:block}.visit-wrapper{display:flex;justify-content:center;align-items:center;flex-direction:column;gap:20px}.visit-input{width:450px;height:50px;border-radius:10px;background-image:radial-gradient(#979fed,#a0a7ee,#a9b0f0,#b3b8f1,#bcc1f2,#c5c9f4,#ced2f5,#d7daf7,#e0e3f8,#eaebf9,#f3f4fb,#fcfcfc);outline:none;border:none;color:#030013;padding:20px}.my-select{padding:.6em 1.4em .5em .8em}.edit-btn{display:block;width:170px}", ""]);

// exports


/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__API_API_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__form_form_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constans_index_js__ = __webpack_require__(2);





class Card {
    constructor(user, cardColor, doctorObj) {
        this._ELEMENTS = {
            moreBtn: document.createElement('button'),
            delBtn: document.createElement('button')
        }
        this.FIO = user.fullName
        this.desc = user.description
        this.target = user.target
        this.urgency = user.urgency
        this.doctor = user.doctor
        this.cardColor = cardColor
        this.diseases = user.diseases
        this.massIndex = user.bodyMassIndex
        this.age = user.age
        this.ID = user.id
        this.doctorObj = doctorObj
    }

    render(parent) {
        let {moreBtn, delBtn} = this._ELEMENTS
        parent.insertAdjacentHTML('afterbegin', `${__WEBPACK_IMPORTED_MODULE_2__constans_index_js__["c" /* makeCardCarcass */](this.ID, this.cardColor, this.FIO, this.doctor)}`)
        if (this.doctor === 'Cardiologist') {
            this.doctorObj.showCardiologist(document.body.querySelector('.first-half'))
        }
        if (this.doctor === 'Dentist') {
            this.doctorObj.showDentist(document.body.querySelector('.first-half'))
        }
        if (this.doctor === "Therapist") {
            this.doctorObj.showTherapist(document.body.querySelector('.first-half'))
        }
        delBtn.classList.add('btn-close')
        delBtn.classList.add('delete-card')
        moreBtn.classList.add('btn-more')
        moreBtn.classList.add('show-more')
        document.body.querySelector('.second-half').insertAdjacentElement('afterend', moreBtn)
        document.querySelector('.card').prepend(delBtn)
        moreBtn.addEventListener('click', () => {
            if (moreBtn.classList.contains('show-more')) {
                moreBtn.classList.toggle('show-more')
                document.getElementById(`${this.ID}`).querySelector('.second-half').style.display = 'block'
            } else {
                moreBtn.classList.toggle('show-more')
                document.getElementById(`${this.ID}`).querySelector('.second-half').style.display = 'none'
            }
        })
        delBtn.addEventListener('click', () => {
            if (confirm("Are u sure?")) {
                __WEBPACK_IMPORTED_MODULE_0__API_API_js__["a" /* default */].removeCards(this.ID)
                document.getElementById(`${this.ID}`).remove()
            }
        })
        document.querySelector('.edit-img').addEventListener('click', () => {
            if (document.querySelector('#card-modal .visit-wrapper')) {
                document.querySelector('#card-modal .visit-wrapper').remove()
                this.editCard()
            } else {
                this.editCard()
            }
        })
    }

    editCard() {
        const editForm = new __WEBPACK_IMPORTED_MODULE_1__form_form_js__["a" /* default */]()
        document.querySelector('.modal').style.display = 'block'
        document.querySelector('#card-modal').classList.add('show')
        editForm.visit(document.querySelector('.modal-content'), 'card-modal')
        editForm.editCard(this.ID)

    }

    madeColor(urgency) {
        const colorBg = {
            Low: 'linear-gradient(to bottom, #336600 0%, #ffcc99 100%)',
            Normal: 'linear-gradient(to bottom, #996633 0%, #ffcc99 100%)',
            High: 'linear-gradient(to bottom, #993333 0%, #ffcc99 100%)'
            // Low: 'linear-gradient(to bottom, #3399ff 0%, #ffffff 100%)',
            // Normal: 'linear-gradient(to bottom, #3366cc 0%, #ffffff 100%)',
            // High: 'linear-gradient(to bottom, #003399 0%, #ffffff 100%)'

        }
        if (urgency === 'Low') {
            return colorBg.Low
        } else if (urgency === 'Normal') {
            return colorBg.Normal
        } else if (urgency === 'High') {
            return colorBg.High
        }
    }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = Card;



/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__first_screen_first_screen_css__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__first_screen_first_screen_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__first_screen_first_screen_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__modal_modal_js__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__form_form_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__form_form_css__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__form_form_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__form_form_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__form_formVisit_css__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__form_formVisit_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__form_formVisit_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__visit_visit_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__visit_visitTherapist_js__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__visit_visitCardiologist_js__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__visit_visitDentist_js__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__Card_card_css__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__Card_card_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__Card_card_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__filters_filters_css__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__filters_filters_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__filters_filters_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__filters_filter_js__ = __webpack_require__(5);














const loginModal = new __WEBPACK_IMPORTED_MODULE_1__modal_modal_js__["a" /* default */]('login-modal')
const newVisitModal = new __WEBPACK_IMPORTED_MODULE_1__modal_modal_js__["a" /* default */]('card-modal')

const form = new __WEBPACK_IMPORTED_MODULE_2__form_form_js__["a" /* default */]()
const filter = new __WEBPACK_IMPORTED_MODULE_11__filters_filter_js__["a" /* Filters */]()
const logInBtn = document.querySelector('#log-in')
const logOutBtn = document.querySelector('#log-out')
const addBtn = document.querySelector('#add-btn')


document.addEventListener('DOMContentLoaded', async () => {
    if (localStorage.getItem('userToken')) {
        logInBtn.classList.remove('active-btn')
        logOutBtn.classList.add('active-btn')
        document.querySelector('#img').style.display = 'none'
        document.querySelector('#add-btn').classList.add('active-btn')
        document.querySelector('.filters').style.display = 'flex'
        await form.checkCards()
        filter.setUpFilter()
    }
})

loginModal.render()
newVisitModal.render()
form.handleClick('login-modal')

logInBtn.addEventListener('click', () => {
    loginModal.appendBody(form.render())
})

logOutBtn.addEventListener('click', () => {
    document.querySelector('#add-btn').classList.remove('active-btn')
    document.querySelector('#img').style.display = 'block'
    document.querySelector('#msg').classList.remove('active-msg')
    document.querySelectorAll('.card').forEach(card => card.remove())
    localStorage.removeItem('userToken')
    document.querySelector('.filters').style.display = 'none'
    toggleBtn()
})

addBtn.addEventListener('click', () => {

    if (document.querySelector('#card-modal .visit-wrapper')) {
        document.querySelector('#card-modal .visit-wrapper').remove()
        const newForm = new __WEBPACK_IMPORTED_MODULE_2__form_form_js__["a" /* default */]()
        document.querySelector('.modal').style.display = 'block'
        newForm.visit(document.querySelector('.modal-content'), 'card-modal')
    } else {
        const newForm = new __WEBPACK_IMPORTED_MODULE_2__form_form_js__["a" /* default */]()
        document.querySelector('.modal').style.display = 'block'
        newForm.visit(document.querySelector('.modal-content'), 'card-modal')
    }

})

function toggleBtn() {
    logInBtn.classList.toggle('active-btn')
    logOutBtn.classList.toggle('active-btn')
}


/***/ })
/******/ ]);